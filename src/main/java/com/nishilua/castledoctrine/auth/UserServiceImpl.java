/*******************************************************************************
 * Copyright 2018 Alfonso Nishikawa
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package com.nishilua.castledoctrine.auth;

import java.util.Collection;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service("userService")
public class UserServiceImpl implements UserService {

    @PersistenceContext
    private EntityManager em ;
    
    @Autowired
    private UserRepository userRepository ;
    
    @Override
    public User get(String username) {
        User user = this.userRepository.getByUsername(username) ;
        if (user == null) {
        	new RuntimeException("User " + username + " does not exists") ;
        }
        return user ;
    }

    @Override
    public Collection<User> findAll() {
        return this.userRepository.findAll() ;
    }

    @Override
    public User findByEmail(String email) {
        return this.userRepository.findByEmail(email) ;
    }

    @Override
    public Boolean exists(Integer id) {
        return this.userRepository.existsById(id);
    }
    
    @Override
    public User getReference(Integer id) {
        return this.userRepository.getOne(id) ;
    }

    @Override
    public User save(User user) {
        return this.userRepository.save(user) ;
    }

    @Override
    public void delete(User user) {
        this.userRepository.delete(user);
    }

    @Override
    public User saveNewUser(User user) {
        if (this.userRepository.existsById(user.getId())) {
            throw new RuntimeException("User " + user.getUsername() + " already exists");            
        }
        BCryptPasswordEncoder bcryptEncoder = new BCryptPasswordEncoder();
        user.setPassword(bcryptEncoder.encode(user.getPassword()));
        return this.userRepository.save(user);
    }

    @Override
    public User getMainData(String username) {
        return this.userRepository.getByUsername(username) ;
    }

}
