
package com.nishilua.castledoctrine.auth;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Email;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.nishilua.castledoctrine.entities.House;

/**
 * Entity implementation class for Entity: User
 */
@Entity
@Table(name="users", uniqueConstraints= {@UniqueConstraint(columnNames= {"id"})})
@JsonIdentityInfo(generator=ObjectIdGenerators.PropertyGenerator.class, property="id", scope=User.class)
public class User implements Serializable {

    private static final long serialVersionUID = -6521122333173751908L;

    @Id
    @NotNull
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    
    @NotNull
    @Size(min=5, max=256)
    private String username;
    
    @JsonIgnore
    @NotNull
    private String password;
    
    @Email
    @Size(min=7, max=50)
    private String email;
    
    private String characterName ;

    private String wifeName ;

    private String sonName ;

    private String daughterName ;
    
    private Integer livesLeft;
    
    private Long lastRobbedOwnerId;

    private String lastRobberyResponse;

    private Long lastRobberyDeadline;
    
    private Integer lastPriceListNumber;

    /** Loot carried back from latest robbery, not deposited in vault
     * yet.  Deposited when house is next checked out for editing.
     */
    private Integer carriedLootValue ;
    
    private String carriedVaultContents ;
    
    private String carriedGalleryContents ;
    
    @NotNull
    private Boolean admin;

    @NotNull
    private Boolean enabled;
    
    @OneToMany(cascade=CascadeType.ALL, mappedBy="user", orphanRemoval=true, fetch=FetchType.LAZY)
    private Set<House> houses;
    
    public User() {
        super();
    }
    
    public User(String name, String password, String email, Boolean enabled) {
        this.username = name ;
        this.password = password ;
        this.email = email ;
        this.enabled = enabled ;
    }

   public User(String name, String password, String email, Boolean enabled, String imageUrl) {
        this.username = name ;
        this.password = password ;
        this.email = email ;
        this.enabled = enabled ;
    }

    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
    
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    
    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }
    
    public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getLivesLeft() {
		return livesLeft;
	}

	public void setLivesLeft(Integer livesLeft) {
		this.livesLeft = livesLeft;
	}

	public Long getLastRobbedOwnerId() {
		return lastRobbedOwnerId;
	}

	public void setLastRobbedOwnerId(Long lastRobbedOwnerId) {
		this.lastRobbedOwnerId = lastRobbedOwnerId;
	}

	public String getLastRobberyResponse() {
		return lastRobberyResponse;
	}

	public void setLastRobberyResponse(String lastRobberyResponse) {
		this.lastRobberyResponse = lastRobberyResponse;
	}

	public Long getLastRobberyDeadline() {
		return lastRobberyDeadline;
	}

	public void setLastRobberyDeadline(Long lastRobberyDeadline) {
		this.lastRobberyDeadline = lastRobberyDeadline;
	}

	public Boolean getAdmin() {
		return admin;
	}

	public void setAdmin(Boolean admin) {
		this.admin = admin;
	}

	public Integer getLastPriceListNumber() {
		return lastPriceListNumber;
	}

	public void setLastPriceListNumber(Integer lastPriceListNumber) {
		this.lastPriceListNumber = lastPriceListNumber;
	}

	public Set<House> getHouses() {
		return houses ;
	}

	public User setHouses(Set<House> houses) {
		this.houses = houses ;
		return this ;
	}

	public String getCharacterName() {
		return characterName;
	}

	public User setCharacterName(String characterName) {
		this.characterName = characterName;
		return this ;
	}

	public String getWifeName() {
		return wifeName;
	}

	public User setWifeName(String wifeName) {
		this.wifeName = wifeName;
		return this ;
	}

	public String getSonName() {
		return sonName;
	}

	public User setSonName(String sonName) {
		this.sonName = sonName;
		return this ;
	}

	public String getDaughterName() {
		return daughterName;
	}

	public User setDaughterName(String daughterName) {
		this.daughterName = daughterName;
		return this ;
	}

	public Integer getCarriedLootValue() {
		return carriedLootValue;
	}

	public User setCarriedLootValue(Integer carriedLootValue) {
		this.carriedLootValue = carriedLootValue;
		return this ;
	}

	public String getCarriedVaultContents() {
		return carriedVaultContents;
	}

	public User setCarriedVaultContents(String carriedVaultContents) {
		this.carriedVaultContents = carriedVaultContents;
		return this ;
	}

	public String getCarriedGalleryContents() {
		return carriedGalleryContents;
	}

	public User setCarriedGalleryContents(String carriedGalleryContents) {
		this.carriedGalleryContents = carriedGalleryContents;
		return this ;
	}

	@Override
    public String toString() {
        return String.format("User - Username: %s, Email: %s, Enabled: %b", this.username, this.email, this.enabled) ;
    }

    @Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((admin == null) ? 0 : admin.hashCode());
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + ((enabled == null) ? 0 : enabled.hashCode());
		result = prime * result + ((lastPriceListNumber == null) ? 0 : lastPriceListNumber.hashCode());
		result = prime * result + ((lastRobbedOwnerId == null) ? 0 : lastRobbedOwnerId.hashCode());
		result = prime * result + ((lastRobberyDeadline == null) ? 0 : lastRobberyDeadline.hashCode());
		result = prime * result + ((lastRobberyResponse == null) ? 0 : lastRobberyResponse.hashCode());
		result = prime * result + ((livesLeft == null) ? 0 : livesLeft.hashCode());
		result = prime * result + ((password == null) ? 0 : password.hashCode());
		result = prime * result + ((username == null) ? 0 : username.hashCode());
		return result;
	}

    @Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (admin == null) {
			if (other.admin != null)
				return false;
		} else if (!admin.equals(other.admin))
			return false;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (enabled == null) {
			if (other.enabled != null)
				return false;
		} else if (!enabled.equals(other.enabled))
			return false;
		if (lastPriceListNumber == null) {
			if (other.lastPriceListNumber != null)
				return false;
		} else if (!lastPriceListNumber.equals(other.lastPriceListNumber))
			return false;
		if (lastRobbedOwnerId == null) {
			if (other.lastRobbedOwnerId != null)
				return false;
		} else if (!lastRobbedOwnerId.equals(other.lastRobbedOwnerId))
			return false;
		if (lastRobberyDeadline == null) {
			if (other.lastRobberyDeadline != null)
				return false;
		} else if (!lastRobberyDeadline.equals(other.lastRobberyDeadline))
			return false;
		if (lastRobberyResponse == null) {
			if (other.lastRobberyResponse != null)
				return false;
		} else if (!lastRobberyResponse.equals(other.lastRobberyResponse))
			return false;
		if (livesLeft == null) {
			if (other.livesLeft != null)
				return false;
		} else if (!livesLeft.equals(other.livesLeft))
			return false;
		if (password == null) {
			if (other.password != null)
				return false;
		} else if (!password.equals(other.password))
			return false;
		if (username == null) {
			if (other.username != null)
				return false;
		} else if (!username.equals(other.username))
			return false;
		return true;
	}

}
