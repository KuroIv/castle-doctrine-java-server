package com.nishilua.castledoctrine;

import java.security.Principal;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;

@RestController
@Api(value = "/reflector")
@ResponseStatus(HttpStatus.OK)
@RequestMapping(path = "/reflector")
public class ReflectorController {

	@RequestMapping(path="/server.php")
	public @ResponseBody String action(@RequestParam Map<String,String> queryParams,
			Principal principal) {
		
		return "http://localhost:8080/castleServer/server.php";
	}

}
