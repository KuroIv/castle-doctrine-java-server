package com.nishilua.castledoctrine;

import java.util.Map;

import com.nishilua.castledoctrine.exceptions.EmailNotFoundException;
import com.nishilua.castledoctrine.exceptions.PermadeadException;
import com.nishilua.castledoctrine.exceptions.UserBlockedException;

public interface ServerService {

	public String serve(String action, Map<String, String> queryParams) throws UserBlockedException, EmailNotFoundException, PermadeadException ;
	
}
