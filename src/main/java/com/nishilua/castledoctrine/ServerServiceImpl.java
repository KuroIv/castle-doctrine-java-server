package com.nishilua.castledoctrine;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.nishilua.castledoctrine.actions.CheckHmac;
import com.nishilua.castledoctrine.actions.CheckUser;
import com.nishilua.castledoctrine.actions.StartEditHouse;
import com.nishilua.castledoctrine.exceptions.EmailNotFoundException;
import com.nishilua.castledoctrine.exceptions.PermadeadException;
import com.nishilua.castledoctrine.exceptions.UserBlockedException;

@Service("serverService")
public class ServerServiceImpl implements ServerService {

	@Value("#{config['version']}")
	private String version;
	
	@Autowired
	CheckUser checkUserAction ;
	
	@Autowired
	CheckHmac checkHmacAction ;
	
	@Autowired
	StartEditHouse startEditHouse ;
	
	public String serve(String action, Map<String, String> queryParams) throws UserBlockedException, EmailNotFoundException, PermadeadException {
		switch (action) {
		case "version" :
			return version;
		case "check_user" :
			return checkUserAction.doCheckUser(queryParams) ;
		case "check_hmac" :
			return checkHmacAction.doCheckHmac(queryParams) ;
		case "start_edit_house" :
			return startEditHouse.doStartEditHouse(queryParams) ;
		}
		
		return null ;
	}

}
