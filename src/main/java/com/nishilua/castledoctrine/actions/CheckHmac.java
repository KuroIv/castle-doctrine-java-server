package com.nishilua.castledoctrine.actions;

import java.util.Map;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.nishilua.castledoctrine.auth.User;
import com.nishilua.castledoctrine.auth.UserRepository;
import com.nishilua.castledoctrine.exceptions.EmailNotFoundException;
import com.nishilua.castledoctrine.exceptions.PermadeadException;
import com.nishilua.castledoctrine.exceptions.UserBlockedException;

@Component
public class CheckHmac {

	@SuppressWarnings("unused")
	private static Logger logger = LoggerFactory.getLogger(CheckHmac.class);
	
	@Autowired
	CheckUser checkUserService;
	
	@Autowired
	UserRepository userRepository;
	
	public String doCheckHmac(Map<String, String> queryParams) throws UserBlockedException, EmailNotFoundException, PermadeadException {

		Integer userId = Integer.valueOf(queryParams.get("user_id")) ;
		Optional<User> optUser = userRepository.findById(userId); 
		if (!optUser.isPresent()) {
			return "";
		}
		
		User user = optUser.get() ;
		checkUserService.checkPermadead(user);
		
		return "OK";
	}

}
