package com.nishilua.castledoctrine.actions;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.nishilua.castledoctrine.auth.UserRepository;
import com.nishilua.castledoctrine.entities.HouseRepository;

@Component
public class ListHouses {

	@Autowired
	UserRepository userRepository ;
	
	@Autowired
	HouseRepository houseRepository ;
	
	/**
	 * 
	 * {action=list_houses, limit=8, name_search=, user_id=1, sequence_number=2, ticket_hmac=9185BE617}
	 * 
	 * @param queryParams
	 * @return
	 */
	public String doListHouses(Map<String, String> queryParams) {
		
		String skip = queryParams.get("skip") ;
		String limit = queryParams.get("limit") ;
		String nameSearch = queryParams.get("name_search") ;
		String addToIgnoreList = queryParams.get("add_to_ignore_list") ;
		String clearIgnoreList = queryParams.get("clear_ignore_list") ;
		
		//TODO WORK IN PROGRESS
		return null;
		
	}

	
}
