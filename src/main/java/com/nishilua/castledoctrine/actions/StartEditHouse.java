package com.nishilua.castledoctrine.actions;

import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.nishilua.castledoctrine.auth.User;
import com.nishilua.castledoctrine.auth.UserRepository;

@Component
public class StartEditHouse {

	@Autowired
	UserRepository userRepository;
	
	public String doStartEditHouse(Map<String, String> queryParams) {
		
		Integer userId = Integer.valueOf(queryParams.get("user_id")) ;
		Optional<User> optUser = userRepository.findById(userId); 
		if (!optUser.isPresent()) {
			return "";
		}
		
		User user = optUser.get() ;
		
		return "DENIED" ;
	}

	
}
