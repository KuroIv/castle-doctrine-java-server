package com.nishilua.castledoctrine.actions;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.nishilua.castledoctrine.auth.User;
import com.nishilua.castledoctrine.auth.UserRepository;
import com.nishilua.castledoctrine.exceptions.EmailNotFoundException;
import com.nishilua.castledoctrine.exceptions.PermadeadException;
import com.nishilua.castledoctrine.exceptions.UserBlockedException;

@Component
public class CheckUser {

	@Value("#{config['version']}")
	private String version;
	
	@Autowired
	UserRepository userRepository;
	
	public String doCheckUser(Map<String, String> queryParams) throws UserBlockedException, EmailNotFoundException, PermadeadException {

		String email = queryParams.get("email") ;
		User user = userRepository.findByEmail(email) ; 
		if (user == null) {
			throw new EmailNotFoundException();
		}
		
		if (user.getEnabled() == false) {
			throw new UserBlockedException() ;
		}
		
		checkPermadead(user) ;
		
		return createOkResponse(user) ;
	}
	
	public void checkPermadead(User user) throws PermadeadException {
		if (user.getLivesLeft() == 0) {
			
			// TODO Delete the house of this user from de DB
			// Or maybe just create a new one?
			// Original from PHP:
			// cd_queryDatabase( "DELETE from $tableNamePrefix"."houses ".
            //        "WHERE user_id = '$user_id';" );
			// cd_queryDatabase( "COMMIT;" );
			throw new PermadeadException() ;
		}
	}
	
	private String createOkResponse(User user) {
		return version + " " + user.getId() + " 0 " + (user.getAdmin()?"1":"0") + " OK" ;
	}
	
}
