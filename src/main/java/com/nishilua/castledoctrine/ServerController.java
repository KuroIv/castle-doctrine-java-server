package com.nishilua.castledoctrine;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.nishilua.castledoctrine.exceptions.EmailNotFoundException;
import com.nishilua.castledoctrine.exceptions.PermadeadException;
import com.nishilua.castledoctrine.exceptions.UserBlockedException;

import io.swagger.annotations.Api;

@RestController
@Api(value = "server")
@ResponseStatus(HttpStatus.OK)
@RequestMapping(path = "/castleServer")
public class ServerController {

	Logger logger = LoggerFactory.getLogger(ServerController.class);
	
	@Autowired
	ServerService serverService;
	
	@RequestMapping(path="/server.php")
	public @ResponseBody String action(@RequestParam Map<String,String> queryParams) {
		
		logger.debug("Request : " + queryParams.toString()) ;
		String action = queryParams.get("action") ;
		
		// TODO check hmac...
		
		try {
			String response = serverService.serve(action, queryParams) ;
			logger.debug("Response: " + response) ;
			return response ;
		} catch (UserBlockedException | EmailNotFoundException e) {
			logger.debug("Response: DENIED");
			return "DENIED" ;
		} catch (PermadeadException e) {
			logger.debug("Response: PERMADEAD");
			return "PERMADEAD" ;
		} catch (RuntimeException e) {
			logger.error("Unexpected exception", e) ;
			throw e ;  
		}

	}

}
