create table users (
	id integer not null primary key,
	username varchar(256) not null,
	password varchar(256) not null,
	email varchar(256),
	
  	character_name varchar(256),
	wife_name varchar(256),
	son_name varchar(256),
	daughter_name varchar(256),
	lives_left integer not null,
	last_robbed_owner_id integer,
	last_robbery_response text,
	last_robbery_deadline integer,
	admin boolean not null,
	last_price_list_number integer,
    -- loot carried back from latest robbery, not deposited in vault
    -- yet.  Deposited when house is next checked out for editing.
    carried_loot_value integer,
    carried_vault_contents text,
    carried_gallery_contents text,
	
	enabled boolean not null
) WITHOUT ROWID ;

create table authorities (
	username varchar(50) not null,
	authority varchar(50) not null,
	primary key (username, authority),
	constraint fk_authorities_users foreign key(username) references users(username)
) WITHOUT ROWID ;