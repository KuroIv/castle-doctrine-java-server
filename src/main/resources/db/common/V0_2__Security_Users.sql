-- bcrypt generator: http://bcrypthashgenerator.apphb.com
INSERT INTO users (id, username, password, email, lives_left, last_robbed_owner_id, last_robbery_response, last_robbery_deadline, admin, last_price_list_number, enabled)
VALUES
(1,'admin','$2a$10$jsWSIQjF5aW2k3knOhDkBuy24BlxZ8gJUbP5tbJjzXckQJcmOyNxK','admin@no.com', -1, -1, '', 0, 1, 0, 1); -- admin:admin
	
--	('alfonso','$2a$06$1hwx3MXIuqAHjUnB3vtSsOrG3H9nIEI8hRhu3jY4M8fnmqdWHtcb6', 'a.a@gmail.com', 1), -- alfonso:aaaaa
--	('pepe1', '$2a$06$8w7Ms8DsZjfEOXJ8wW9BMOqJ7VU5bvexrr7Ap.VGBJEnvMJChLyNi', 'pepe@no.com', 1), -- pepe1:bbbbb
--	('jose1', '$2a$06$227NNFCpyk0QHcxg7oFWXunddSXoiIMNfiYCco8IiZio.4jHpcSV6', 'jose@no.com', 1) -- jose1:ccccc
;

INSERT INTO authorities(username, authority)
VALUES
	('admin', 'ROLE_ADMIN')
;